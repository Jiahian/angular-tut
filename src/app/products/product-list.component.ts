import { Component, OnInit } from '@angular/core';
import { IProduct } from './product';
import { ProductService } from './product.service';

@Component({
    //selector:'pm-products', //No need this anymore as you use routing
    templateUrl:'./product-list.component.html',
    styleUrls: ['./product-list.component.css']
})

export class ProductListComponent implements OnInit { //need to import OnInit from @angular/core
    pageTitle: string = 'Product List!';
    imageWidth: number = 50;
    imageMargin: number = 2;
    showImage: boolean = false;
    errorMessage: string;
    _listFilter: string; 
  
    get listFilter(): string { // need to do a getter and setter
      return this._listFilter;
    }
    set listFilter(value: string){ //when user modifies the value, passing in the changed value
      this._listFilter = value;
      this.filteredProducts = this.listFilter ? this.performFilter(this.listFilter) : this.products;
    }

    filteredProducts: IProduct[];
    products: IProduct[] =[];

    constructor(private productService: ProductService) { //executed when the component is first initialized. 
    }
    
    onRatingClicked(message:string):void{
      this.pageTitle = 'Product List: ' + message;
    }

    performFilter(filterBy: string): IProduct[] {
      filterBy = filterBy.toLowerCase();
      return this.products.filter((products: IProduct)=>
        products.productName.toLowerCase().indexOf(filterBy) !== -1);
    }

    toggleImage():void{
      this.showImage = !this.showImage;
    }

    ngOnInit(): void {  
      this.productService.getProducts().subscribe({
        next: products => {
          this.products = products,
          this.filteredProducts = this.products
        },
        error: err => this.errorMessage = err
      });
      
      //this.listFilter = 'cart';
    }
}