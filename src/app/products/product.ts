export interface IProduct {
    productId: number;
    productName: string;
    productCode: string;
    releaseDate: string; //can be changed to date
    price: number;
    description: string;
    starRating: number;
    imageUrl: string;
}