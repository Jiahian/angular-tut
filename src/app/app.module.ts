import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core'; //essential to use ng-module for 
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { ProductListComponent } from './products/product-list.component';
import { StarComponent } from './shared/star.component';
import { ConvertToSpacesPipe } from './shared/convert-to-spaces.pipe';
import { ProductDetailComponent } from './products/product-detail.component';
//must always import things and declare them before using them

@NgModule({
  //For declaring components, directives and pieps that belong to this module
  declarations: [
    AppComponent, //So it can be used anywhere in this MODULE
    WelcomeComponent,
    ProductListComponent,
    StarComponent,
    ConvertToSpacesPipe,
    ProductDetailComponent
  ],
  //For pulling in external modules
  imports: [
    BrowserModule, 
    FormsModule, //allows us to interact with browser APIs, etc
    HttpClientModule, 
    RouterModule.forRoot([
      { path: 'products', component: ProductListComponent }, //orders of each route matters, most significant at the top
      { path: 'products/:id', component: ProductDetailComponent },
      { path: 'welcome', component: WelcomeComponent },
      { path: '', redirectTo: 'welcome', pathMatch: 'full' }, //default route
      { path: '**', redirectTo: 'welcome', pathMatch: 'full'} //wildcard route, usually for 404 error page
    ]) //to configure routes
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
