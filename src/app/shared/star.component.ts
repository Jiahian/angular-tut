import { Component, OnChanges, Input, EventEmitter, Output } from '@angular/core';

@Component({
    selector:'pm-star',
    templateUrl:'./star.component.html',
    styleUrls: ['./star.component.css']
})

export class StarComponent implements OnChanges { //use OnChanges as we want the rating to change accordingly
    //use @Input() decorator for the rating no to be passed from parent into the nested component,
    //then use property binding in the html of parent 
    @Input() rating: number; 
    starWidth: number;
    //To pass data from nested component to parent use @Output()
    @Output() ratingClicked: EventEmitter<string> =  //string is the type of event that we wanna pass out
        new EventEmitter<string>(); //create new instance of the Event Property

    ngOnChanges(): void {
        this.starWidth = this.rating * 75/5;
    }

    onClick(): void{
        //emit method to raise the event to the parent.
        //Now the parent need to listen for this event using event binding
        this.ratingClicked.emit(`The rating ${this.rating} was clicked!`);
    }
}
  